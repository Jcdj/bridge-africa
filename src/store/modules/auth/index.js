const state = {
    users: [],
    user:  {
        name: "",
        email: "",
        password: "",
    },
    jwt: {
        token: "",
        tokisNotUseren: "",
        isAuthed: false
      },
}

const getters= {
    user: state => state.user,
    jwt: state => state.jwt,
    isAuthed: state => state.isAuthed,
}

const mutations= {
    SAVE_USER(state, _user) {
        state.user = _user
    },
    SAVE_JWT(state, _jwt) {
        state.jwt = _jwt
    },
}

const actions= {
    saveUser(context, _user){
        context.commit('SAVE_USER', _user);
    },
      saveJWT(context, _jwt){
        context.commit('SAVE_JWT', _jwt);
    },
}

const userModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default userModule;
const state = {
    products: [],
}

const getters= {
    products: state => state.products,
}

const mutations= {
    ADD_PRODUCT(state, _product) {
        state.products.push(_product);
    }, 
    REMOVE_PRODUCT(state, _product) {
        console.log(_product);
        console.log(JSON.stringify(state.products));
        state.products = state.products.filter((p) => {
          console.log(p._product !== _product);
          return p._product !== _product;
        });
        console.log(JSON.stringify(state.products));
    },
}

const actions= {
    saveProduct(context, _product){
        context.commit('ADD_PRODUCT', _product);
      },
      removeProduct(context, _product){
        context.commit('REMOVE_PRODUCT', _product);
      },
}

const userModule = {
    state,
    mutations,
    actions,
    getters
}
  
export default userModule;
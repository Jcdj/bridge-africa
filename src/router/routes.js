import { createWebHistory, createRouter } from 'vue-router'
import Login from '../pages/Auth/Login.vue'
import Register from '../pages/Auth/Register.vue'
import Product from '../pages/Product/Product.vue'
import Home from '../pages/Home.vue'

// import store from '../store'
    
/* const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/product')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }
    next('/connexion')
} */

const routes = [
    {
        path: '',
        redirect: { name: 'connexion'}
    },
    // {path: '/connexion', component: Login, name: 'connexion',  beforeEnter: ifNotAuthenticated},
    // {path: '/inscription', component: Register, name: 'inscription',  beforeEnter: ifNotAuthenticated},
    {path: '/connexion', component: Login, name: 'connexion'},
    {path: '/inscription', component: Register, name: 'inscription'},
    
    {
        path: '/',
        component: Home,
        // beforeEnter: ifAuthenticated,
        children: [
            {
                path: '/product' , component: Product, name: 'product'
            },  
        ]
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router;
import { createApp } from 'vue'
import App from './App.vue'
import router from './router/routes.js'
import store from './store'

import 'bootstrap'
// import 'bootstrap/dist/css/bootstrap.min.css'
// import 'jquery'

const app = createApp(App);

app.use(store);
app.use(router);
app.mount('#app');